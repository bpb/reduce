reduce - Array.prototype.reduce in pre-ES5 JavaScript
=====================================================

`reduce(array, callback)` returns a single value, which is the result of applying
`callback` to each element in `array`.

Syntax
------

    reduce(array, callback[, initialValue])

Parameters
----------

- `array`   : The [Array] to reduce.
- `callback`: A [Function] to execute on each value in the array, taking four
              arguments:

  - `previousValue`: The value previously returned in the last invocation of
                     `callback`. If this is the first iteration,
                     `previousValue` will be the first element in the array or
                     `initialValue` if supplied.
                     (See [Description](#description) below.)
  - `currentValue` : The current array element being processed.
  - `index`        : A [Number] identifying the index of the current element
                     being processed in the array.
  - `array`        : The [Array] reduce was called upon.

- `initialValue`: Optional. The [Object] to use as `previousValue` for the
                  first call of `callback`.

Examples
--------

### Sum up all values within an array

```js
var total = reduce([0, 1, 2, 3], function(a, b) {
  return a + b;
});
// total == 6
```

### Flatten an array of arrays

```js
var flattened = reduce([[0, 1], [2, 3], [4, 5]], function(a, b) {
  return a.concat(b);
});
// flattened is [0, 1, 2, 3, 4, 5]
```

### Count the occurance of each word in an array of strings

```js
var lines = [
  "hey",
  "hey you there",
  "you wanna get some pizza"
];
var occurances = reduce(lines, function (occur, line) {
    line.split(' ').forEach(function (word) {
        occur[word] = ~~occur[word] + 1;
    });
    return occur;
}, {});
```

Description
-----------

`reduce` executes the callback function once for each element present in the
array, excluding holes in the array, receiving four arguments: the initial
value (or value from the previous callback call), the value of the current
element, the current index, and the array over which iteration is occurring.

The first time the callback is called, `previousValue` and `currentValue` can
be one of two values. If `initialValue` (the optional third element) is
provided in the call to reduce, then `previousValue` will be equal to
`initialValue` and `currentValue` will be equal to the first value in the
array. If no `initialValue` was provided, then `previousValue` will be equal
to the first value in the array and `currentValue` will be equal to the
second.

If the array is empty and no initialValue was provided, TypeError would be
thrown. If the array has only one element (regardless of position) and no
initialValue was provided, or if initialValue is provided but the array is
empty, the solo value would be returned without calling callback.

Setup
-----

This project is a stub and is intended for you to clone and [write the
necessary implementation](reduce.js#L124) for the `reduce` method:

```sh
git clone https://gitlab.com/bpb/reduce.git
cd reduce/
npm install
npm test
```

You can review the test expectations either in [SPEC.md](SPEC.md) or
[test/index.js](test/index.js).

See Also
--------
- [Array]
- [Array#reduce](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce)
- [ES5 Spec](http://es5.github.io/#x15.4.4.21)

[Array]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array
[Function]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/function
[Number]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number
[Object]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object

License
-------
[The ISC License (ISC)](http://opensource.org/licenses/ISC)