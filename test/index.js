/* global describe it */
var expect = require('expect.js')
var reduce = require('../reduce')

describe('reduce', function() {
    it('should be a function', function() {
        expect(reduce).to.be.a('function')
    })

    it('should take an array and a reducer function, and return a single value', function() {
        expect(reduce(['c', 'a', 't'], concat)).to.equal('cat')
    })

    it('should execute `callback` for each element present in the array, and return what `callback` returns on it\'s last iteration', function() {
        expect(reduce([1, 2, 3], sum)).to.equal(6)
    })

    it('should throw a TypeError if `callback` is not passed, or is not executable', function() {
        expect(reduce).withArgs([1, 2]).to.throwException(function(err) {
            expect(err).to.be.a(TypeError)
        })
    })

    it('should accept an optional third parameter as an initial value', function() {
        expect(reduce([1, 2, 3], sum, 10)).to.equal(16)
    })

    it('should pass `previousValue`, `currentValue`, `index`, and `array` to `callback`', function() {
        var initialArray = [10, 20]

        plan(4)
        reduce(initialArray, function(previousValue, currentValue, index, array) {
            expect(previousValue).to.equal(10)
            expect(currentValue).to.equal(20)
            expect(index).to.equal(1)
            expect(array).to.equal(initialArray)
        })
    })

    it('should use `initialValue` as the `previousValue` the first time `callback` is called if `initialValue` is provided', function() {
        plan(2)
        reduce([2], function(previousValue, currentValue) {
            expect(previousValue).to.equal(1)
            expect(currentValue).to.equal(2)
        }, 1)
    })

    it('should use the first item in array as `previousValue` and the second as `currentValue` the first time `callback` is called if no `initialValue` is provided', function() {
        plan(2)
        reduce([1, 2], function(previousValue, currentValue) {
            expect(previousValue).to.equal(1)
            expect(currentValue).to.equal(2)
        })
    })

    it('should throw TypeError if `array` is empty and no `initialValue` is provided', function() {
        plan(7)
        expect(reduce).withArgs(null, noop).to.throwException(function(err) {
            expect(err).to.be.a(TypeError)
            //expect(e.message).to.equal('reduce called on null or undefined array')
        })

        expect(reduce).withArgs(undefined, noop).to.throwException(function(err) {
            expect(err).to.be.a(TypeError)
            //expect(e.message).to.equal('reduce called on null or undefined array')
        })

        expect(reduce).withArgs([], noop).to.throwException(function(err) {
            expect(err).to.be.a(TypeError)
            //expect(e.message).to.equal('reduce of empty array with no initial value')
        })

        expect(reduce).withArgs([], noop, 'initial value').to.not.throwException()
    })

    it('should return the solo array value without calling `callback` if the array has only one element and no `initialValue` is provided', function() {
        // Single array element is returned unchanged.
        expect(reduce(['one'], concatAndUppercase)).to.equal('one')

        // But multiples are reduced by the function.
        expect(reduce(['one', 'two'], concatAndUppercase)).to.equal('ONETWO')
    })

    it('should return `initialValue` without calling `callback` if `initialValue` is provided and the array is empty', function() {
        // Initial value is returned unchanged.
        expect(reduce([], concatAndUppercase, 'initial')).to.equal('initial')

        // But multiple values (single array element + initial value) are reduced.
        expect(reduce(['one'], concatAndUppercase, 'initial')).to.equal('INITIALONE')
    })

    it('should skip holes in the array', function() {
        expect(reduce(['c', 'a', , , 't'], concatAndUppercase)).to.equal('CAT')
        expect(reduce([, 'c', , 'a', , 't', ], concatAndUppercase)).to.equal('CAT')
    })
})

function noop() {}

function sum(a, b) {
    return a + b
}

function concat(a, b) {
    return '' + a + b
}

function concatAndUppercase(a, b) {
    return (a + b).toUpperCase()
}



// Mocha doesn't have built-in support for assertion counting :(
/* global afterEach beforeEach */
var expected, asserted
var _expect = expect
expect = function() {
    var result = _expect.apply(this, arguments)
    if (expected) {
        asserted++
    }
    return result
}
function plan(n) {
    expected = n
}
function reset() {
    asserted = expected = 0
}
function check() {
    if (expected && (expected !== asserted)) {
        this.test.error(new Error('expected ' + expected + ' assertions but ' + asserted + ' passed'))
    }
    asserted = expected = 0
}
beforeEach(reset)
afterEach(check)
