# TOC
   - [reduce](#reduce)
<a name=""></a>
 
<a name="reduce"></a>
# reduce
should be a function.

```js
expect(reduce).to.be.a('function')
```

should take an array and a reducer function, and return a single value.

```js
expect(reduce(['c', 'a', 't'], concat)).to.equal('cat')
```

should execute `callback` for each element present in the array, and return what `callback` returns on it's last iteration.

```js
expect(reduce([1, 2, 3], sum)).to.equal(6)
```

should throw a TypeError if `callback` is not passed, or is not executable.

```js
expect(reduce).withArgs([1, 2]).to.throwException(function(err) {
    expect(err).to.be.a(TypeError)
})
```

should accept an optional third parameter as an initial value.

```js
expect(reduce([1, 2, 3], sum, 10)).to.equal(16)
```

should pass `previousValue`, `currentValue`, `index`, and `array` to `callback`.

```js
var initialArray = [10, 20]
plan(4)
reduce(initialArray, function(previousValue, currentValue, index, array) {
    expect(previousValue).to.equal(10)
    expect(currentValue).to.equal(20)
    expect(index).to.equal(1)
    expect(array).to.equal(initialArray)
})
```

should use `initialValue` as the `previousValue` the first time `callback` is called if `initialValue` is provided.

```js
plan(2)
reduce([2], function(previousValue, currentValue) {
    expect(previousValue).to.equal(1)
    expect(currentValue).to.equal(2)
}, 1)
```

should use the first item in array as `previousValue` and the second as `currentValue` the first time `callback` is called if no `initialValue` is provided.

```js
plan(2)
reduce([1, 2], function(previousValue, currentValue) {
    expect(previousValue).to.equal(1)
    expect(currentValue).to.equal(2)
})
```

should throw TypeError if `array` is empty and no `initialValue` is provided.

```js
plan(7)
expect(reduce).withArgs(null, noop).to.throwException(function(err) {
    expect(err).to.be.a(TypeError)
    //expect(e.message).to.equal('reduce called on null or undefined array')
})
expect(reduce).withArgs(undefined, noop).to.throwException(function(err) {
    expect(err).to.be.a(TypeError)
    //expect(e.message).to.equal('reduce called on null or undefined array')
})
expect(reduce).withArgs([], noop).to.throwException(function(err) {
    expect(err).to.be.a(TypeError)
    //expect(e.message).to.equal('reduce of empty array with no initial value')
})
expect(reduce).withArgs([], noop, 'initial value').to.not.throwException()
```

should return the solo array value without calling `callback` if the array has only one element and no `initialValue` is provided.

```js
// Single array element is returned unchanged.
expect(reduce(['one'], concatAndUppercase)).to.equal('one')
// But multiples are reduced by the function.
expect(reduce(['one', 'two'], concatAndUppercase)).to.equal('ONETWO')
```

should return `initialValue` without calling `callback` if `initialValue` is provided and the array is empty.

```js
// Initial value is returned unchanged.
expect(reduce([], concatAndUppercase, 'initial')).to.equal('initial')
// But multiple values (single array element + initial value) are reduced.
expect(reduce(['one'], concatAndUppercase, 'initial')).to.equal('INITIALONE')
```

should skip holes in the array.

```js
expect(reduce(['c', 'a', , , 't'], concatAndUppercase)).to.equal('CAT')
expect(reduce([, 'c', , 'a', , 't', ], concatAndUppercase)).to.equal('CAT')
```

